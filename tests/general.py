#!/usr/bin/env python3

from o3test import *
import random

ZX = PyZPolyClass()

def rand_poly(n):
  return ZX([random.randrange(-3, 4) for _ in range(n-1)] + [1])


a = rand_poly(7)
b = rand_poly(5)
c = rand_poly(3)
