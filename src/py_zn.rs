use pyo3::basic::CompareOp;
use pyo3::{exceptions, prelude::*};
use std::sync::Arc;
use paste::paste;

use super::*;

make_py_class!(PyZMod, ZMod, ZModElem);
type ZModArg<'a> = IntArg<'a,PyRef<'a,PyZMod>>;



impl<'a> AsRef<ZZ> for ZModArg<'a> {
  fn as_ref(&self) -> &ZZ {
    match self {
      ZModArg::Obj(o) => o.el.rep(),
      ZModArg::ZZ(o) => &o.el,
      ZModArg::Lit(l) => &l,
    }
  }
}


#[pymethods]
impl PyZModClass {
  #[new]
  fn create(arg: ZZArg) -> PyResult<Self> {
    ZMod::new(ZZ::from(arg))
      .map(PyZModClass::new)
      .ok_or_else(|| exceptions::PyRuntimeError::new_err("failed"))
  }

  fn __call__(&self, arg: Option<ZZArg>) -> PyResult<PyZMod> {
    let el = self.alg.from_integer(arg.map(ZZArg::into).unwrap_or(ZZ::default()));
    Ok(PyZMod::new(Arc::clone(&self.alg), el))
  }

  // ObjectProtocol
  //
  // MISSING: __format__, __getattr__, __setattr__, __delattr__, __bytes__

  fn __str__(&self) -> String {
    format!("Zn({})", self.alg.rep().to_string())
  }

  fn __repr__(&self) -> String {
    format!("Zn({})", self.alg.rep().to_string())
  }

  fn __hash__(&self) -> u64 {
    ZZ::from(self.alg.rep() % 0x1fffffffffffffffu64).to_u64_wrapping()
  }

  fn __richcmp__(&self, other: &PyZModClass, op: CompareOp) -> bool {
    PyZZ::compare_zz(op, &self.alg.rep(), &other.alg.rep())
  }

  fn __bool__(&self) -> bool {
    true
  }
}

// impl<'a> PyZModArg<'a> {
//   fn zn_ref(&self) -> Result<&PyZMod,&ZZ> {
//     use PyZModArg::*;
//     match self {
//       Zn(o) => Ok(Deref::deref(o)),
//       ZZ(o) => Err(&o.el),
//       Lit(l) => Err(&l),
//     }
//   }
// }


impl PyZMod {
  fn promote_el(&self, arg: ZModArg) -> ZModElem {
    match arg {
      ZModArg::Obj(o) => o.el.clone(),
      ZModArg::ZZ(o) => self.alg.from_integer(o.el.clone()),
      ZModArg::Lit(l) => self.alg.from_integer(l.0),
    }
  }
}

#[pymethods]
impl PyZMod {
  #[new]
  fn create(a1: ZModArg, a2: Option<ZZArg>) -> PyResult<Self> {
    a1.map_int(
      move |o| Ok(Self::new(Arc::clone(&o.alg), o.el.clone())),
      move |i| ZMod::new(i)
        .ok_or_else(|| exceptions::PyRuntimeError::new_err("failed"))
        .map(Arc::new)
        .and_then(move |alg| {
          a2.ok_or_else(|| exceptions::PyRuntimeError::new_err("need argument"))
            .map(|zarg| alg.from_integer(zarg.into()))
            .map(move |el| Self::new(alg, el))
        })
      )
  }

  // ObjectProtocol
  //
  // MISSING: __format__, __getattr__, __setattr__, __delattr__, __bytes__

  fn __str__(&self) -> String {
    format!("({})", self.alg.neg_rep(&self.el).to_string())
  }

  fn __repr__(&self) -> String {
    format!("({})", self.alg.neg_rep(&self.el).to_string())
  }

  fn __hash__(&self) -> u64 {
    ZZ::from(self.el.rep() % 0x1fffffffffffffffu64).to_u64_wrapping()
  }

  fn __richcmp__(&self, other: ZModArg, op: CompareOp) -> bool {
    let other_el = self.promote_el(other);
    PyZZ::compare_zz(op, &self.el.rep(), &other_el.rep())
  }

  fn __bool__(&self) -> bool {
    self.alg.is_zero(&self.el)
  }

  // NumberProtocol
  //
  fn __neg__(&self) -> PyResult<Self> {
    fwd_to_alg!(self neg)
  }
  fn __add__(lhs: PyRef<Self>, rhs: ZModArg) -> PyResult<Self> {
    fwd_to_alg!(lhs rhs add add_rhs)
  }
  fn __radd__(lhs: PyRef<Self>, rhs: ZModArg) -> PyResult<Self> {
    fwd_to_alg!(lhs rhs add_rhs add)
  }
  fn __sub__(lhs: PyRef<Self>, rhs: ZModArg) -> PyResult<Self> {
    fwd_to_alg!(lhs rhs sub sub_rhs)
  }
  fn __rsub__(lhs: PyRef<Self>, rhs: ZModArg) -> PyResult<Self> {
    fwd_to_alg!(lhs rhs sub_rhs sub)
  }
  fn __invert__(&self) -> PyResult<Self> {
    fwd_to_alg!(self inv)
  }
  fn __mul__(lhs: PyRef<Self>, rhs: ZModArg) -> PyResult<Self> {
    fwd_to_alg!(lhs rhs mul mul_rhs)
  }
  fn __rmul__(lhs: PyRef<Self>, rhs: ZModArg) -> PyResult<Self> {
    fwd_to_alg!(lhs rhs mul_rhs mul)
  }
  fn __truediv__(lhs: PyRef<Self>, rhs: ZModArg) -> PyResult<Self> {
    fwd_to_alg!(lhs rhs div div_rhs)
  }
  fn __rtruediv__(lhs: PyRef<Self>, rhs: ZModArg) -> PyResult<Self> {
    fwd_to_alg!(lhs rhs div_rhs div)
  }
  fn __pow__(lhs: PyRef<Self>, rhs: ZZArg, modulo: Option<ZZArg>) -> PyResult<Self> {
    match modulo {
      Some(m) => {
        let alg = ZMod::new(m)
          .ok_or_else(|| exceptions::PyRuntimeError::new_err("failed"))?;
        let el = alg.power(alg.from_integer(lhs.el.rep().clone()), rhs.into())
                    .ok_or_else(|| exceptions::PyRuntimeError::new_err("failed"))?;
        Ok(Self::new(Arc::new(alg), el))
      },
      None => lhs.alg.power(lhs.el.clone(), rhs.into())
                     .ok_or_else(|| exceptions::PyRuntimeError::new_err("failed"))
                     .map(move |el| Self::new(Arc::clone(&lhs.alg), el)),
    }
  }
}


// #[pymethods]
// impl PyVecZZEl {
//   // ObjectProtocol
//   //
//   // MISSING: __format__, __getattr__, __setattr__, __delattr__, __bytes__

//   fn __str__(&self) -> String {
//     format!("({})", self.el.rep().to_string())
//   }

//   fn __repr__(&self) -> String {
//     format!("({})", self.el.rep().to_string())
//   }

//   fn __hash__(&self) -> u64 {
//     ZZ::from(self.el.rep() % 0x1fffffffffffffffu64).to_u64_wrapping()
//   }

//   fn __richcmp__(&self, other: PyZModArg, op: CompareOp) -> bool {
//     match op {
//       CompareOp::Eq => self.alg.rep() == other.as_ref(),
//       CompareOp::Ne => self.alg.rep() != other.as_ref(),
//       CompareOp::Gt => self.alg.rep() > other.as_ref(),
//       CompareOp::Ge => self.alg.rep() >= other.as_ref(),
//       CompareOp::Lt => self.alg.rep() < other.as_ref(),
//       CompareOp::Le => self.alg.rep() <= other.as_ref(),
//     }
//   }

//   fn __bool__(&self) -> bool {
//     self.alg.is_zero(&self.el)
//   }
// }
