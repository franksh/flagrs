use pyo3::{PyClass, prelude::*};
use rnt::ZZ;
use crate::{RugInteger, py_zz::PyZZ};

#[derive(FromPyObject)]
pub enum IntArg<'a,T> {
  Obj(T),
  ZZ(PyRef<'a,PyZZ>),
  Lit(RugInteger),
}

impl<'a,T: PyClass> IntArg<'a,PyRef<'a,T>> {
  #[inline]
  pub fn map_all<F,G,H,R>(self, objf: F, intf: G, litf: H) -> R
  where
    F: FnOnce(&T) -> R,
    G: FnOnce(&ZZ) -> R,
    H: FnOnce(ZZ) -> R,
  {
    use IntArg::*;
    match self {
      Obj(o) => objf(&o),
      ZZ(ref o) => intf(&o.el),
      Lit(l) => litf(l.0),
    }
  }

  #[inline]
  pub fn map_int<F,G,R>(self, objf: F, intf: G) -> R
  where
    F: FnOnce(&T) -> R,
    G: FnOnce(ZZ) -> R,
  {
    use IntArg::*;
    match self {
      Obj(o) => objf(&o),
      ZZ(ref o) => intf(o.el.clone()),
      Lit(l) => intf(l.0),
    }
  }
}
