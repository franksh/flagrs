// use pyo3::{PyClass, exceptions, prelude::*};

use std::{borrow::{Borrow, Cow}, hash::{Hasher, Hash}, sync::Arc};
use pyo3::{basic::CompareOp, exceptions::{self, PyNotImplementedError}};

use super::*;

make_py_class!(PyZPoly, Polynomial<ZRing>, PolyElem<ZZ>);

type ZPolyArg<'p> = IntArg<'p,PyRef<'p,PyZPoly>>;

// int
// ZZ
// [int|ZZ]

impl PyZPoly {
  pub fn promote_zz(&self, val: impl Into<ZZ>) -> PolyElem<ZZ> {
    self.alg.elem(vec![val.into()])
  }

  fn with_arg<F,R>(&self, arg: ZPolyArg, f: F) -> R
  where F: FnOnce(&PolyElem<ZZ>) -> R {
    use IntArg::*;
    match arg {
      Obj(o) => f(&o.el),
      ZZ(o) => {
        let tmp = self.promote_zz(o.el.clone());
        f(&tmp)
      },
      Lit(l) => {
        let tmp = self.promote_zz(l.0);
        f(&tmp)
      },
    }
  }
}

#[pymethods]
impl PyZPolyClass {
  #[new]
  fn create() -> Self {
    Self::new(Polynomial::new(ZRing))
  }

  fn __call__(&self, coeff: Vec<RugInteger>) -> PyZPoly {
    let el = self.alg.elem(unsafe { std::mem::transmute::<Vec<RugInteger>,Vec<ZZ>>(coeff) });
    PyZPoly::new(Arc::clone(&self.alg), el)
  }
}


fn monomial_string(e: usize, c: &ZZ) -> Option<String> {
  if c.cmp0().is_eq() {
    return None
  }
  let mut res = String::new();
  if c.significant_bits() == 1 {
    if c.cmp0().is_lt() { res.push('-'); }
    if e == 0 { res.push('1'); }
  } else {
    res.push_str(&c.to_string());
  };

  if e == 1 {
    res.push_str("x");
  } else if e != 0 {
    res.push_str(&format!("x^{}", e))
  }
  Some(res)
}



#[pymethods]
impl PyZPoly {
  #[new]
  fn create() -> PyResult<Self> {
    unimplemented!()
  }

  // ObjectProtocol
  fn __str__(&self) -> String {
    self.__repr__()
  }

  fn __repr__(&self) -> String {
    self.el.rep()
           .iter()
           .enumerate()
           .rev()
           .map(|(i,x)| monomial_string(i, x))
           .filter_map(|x| x)
           .collect::<Vec<String>>()
      .join(" + ")
  }

  fn __bool__(&self) -> bool {
    self.alg.is_zero(&self.el)
  }

  fn __richcmp__(&self, rhs: ZPolyArg, op: CompareOp) -> PyResult<bool> {
    self.with_arg(rhs, |o| {
      match op {
        CompareOp::Eq => Ok(self.el == *o),
        CompareOp::Ne => Ok(self.el != *o),
        _ => Err(PyNotImplementedError::new_err("invalid comparison")),
      }
    })
  }

  fn __hash__(&self) -> u64 {
    let mut hasher = std::collections::hash_map::DefaultHasher::new();
    self.el.hash(&mut hasher);
    hasher.finish()
  }

  // NumberProtocol
  fn __neg__(&self) -> PyResult<Self> {
    fwd_to_alg!(self neg)
  }
  fn __add__(lhs: PyRef<Self>, rhs: ZPolyArg) -> PyResult<Self> {
    fwd_to_alg!(lhs rhs add add_rhs)
  }
  fn __radd__(lhs: PyRef<Self>, rhs: ZPolyArg) -> PyResult<Self> {
    fwd_to_alg!(lhs rhs add_rhs add)
  }
  fn __sub__(lhs: PyRef<Self>, rhs: ZPolyArg) -> PyResult<Self> {
    fwd_to_alg!(lhs rhs sub sub_rhs)
  }
  fn __rsub__(lhs: PyRef<Self>, rhs: ZPolyArg) -> PyResult<Self> {
    fwd_to_alg!(lhs rhs sub_rhs sub)
  }
  fn __mul__(lhs: PyRef<Self>, rhs: ZPolyArg) -> PyResult<Self> {
    fwd_to_alg!(lhs rhs mul mul_rhs)
  }
  fn __rmul__(lhs: PyRef<Self>, rhs: ZPolyArg) -> PyResult<Self> {
    fwd_to_alg!(lhs rhs mul_rhs mul)
  }

  fn __mod__(lhs: PyRef<Self>, rhs: ZPolyArg) -> PyResult<Self> {
    fwd_to_alg!(lhs rhs rem rem_rhs)
  }
  fn __rmod__(lhs: PyRef<Self>, rhs: ZPolyArg) -> PyResult<Self> {
    fwd_to_alg!(lhs rhs rem_rhs rem)
  }
  fn __floordiv__(lhs: PyRef<Self>, rhs: ZPolyArg) -> PyResult<Self> {
    fwd_to_alg!(lhs rhs div div_rhs)
  }
  fn __rfloordiv__(lhs: PyRef<Self>, rhs: ZPolyArg) -> PyResult<Self> {
    fwd_to_alg!(lhs rhs div_rhs div)
  }
  fn __divmod__(lhs: PyRef<Self>, rhs: ZPolyArg) -> PyResult<(Self,Self)> {
    let alg = Arc::clone(&lhs.alg);
    let (el1,el2) = rhs.map_int(
      |o| alg.divrem(lhs.el.clone(), o.el.clone()),
      |i| alg.divrem(lhs.el.clone(), alg.from_integer(i)),
    ).ok_or_else(|| exceptions::PyRuntimeError::new_err("failed"))?;
    Ok((Self::new(Arc::clone(&lhs.alg), el1), Self::new(alg, el2)))
  }
  fn __rdivmod__(lhs: PyRef<Self>, rhs: ZPolyArg) -> PyResult<(Self,Self)> {
    let alg = Arc::clone(&lhs.alg);
    let (el1,el2) = rhs.map_int(
      |o| alg.divrem_rhs(o.el.clone(), lhs.el.clone()),
      |i| alg.divrem_rhs(alg.from_integer(i), lhs.el.clone()),
    ).ok_or_else(|| exceptions::PyRuntimeError::new_err("failed"))?;
    Ok((Self::new(Arc::clone(&lhs.alg), el1), Self::new(alg, el2)))
  }

  fn __pow__(lhs: PyRef<Self>, rhs: ZZArg, modulo: Option<ZZArg>) -> PyResult<Self> {
    if modulo.is_some() {
      todo!();
    }
    lhs.alg.power(lhs.el.clone(), rhs.into())
            .ok_or_else(|| exceptions::PyRuntimeError::new_err("failed"))
            .map(|el| Self::new(Arc::clone(&lhs.alg), el))
  }
  // __rpow__ doesn't make any sense.
}
