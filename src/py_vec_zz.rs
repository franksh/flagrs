use pyo3::{PyClass, exceptions, prelude::*};
use std::sync::Arc;
use super::*;

make_py_class!(PyZVec, Cartesian<ZRing>, Vec<ZZ>);

#[pymethods]
impl PyZVecClass {
  #[new]
  fn create(size: usize) -> Self {
    assert!(size < 1<<30);
    Self::new(Cartesian::new(ZRing, size))
  }

  fn __call__(&self, coeff: Vec<RugInteger>) -> PyResult<PyZVec> {
    let el = self.alg.elem(unsafe { std::mem::transmute::<Vec<RugInteger>,Vec<ZZ>>(coeff) });
    Ok(PyZVec::new(Arc::clone(&self.alg), el))
  }
}

#[pymethods]
impl PyZVec {

  // ObjectProtocol
  fn __str__(&self) -> String {
    self.__repr__()
  }

  fn __repr__(&self) -> String {
    let mut s = self.el.iter()
           .map(|x| format!("{}", x))
           .collect::<Vec<String>>()
      .join(" ");
    ["<", &s, ">"].concat()
  }

  fn __bool__(&self) -> bool {
    self.alg.is_zero(&self.el)
  }

  // NumberProtocol

  // fn __add__(lhs: PyRef<Self>, rhs: IntArg<PyRef<Self>>) -> PyResult<Self> {
  //   let alg = Arc::clone(&lhs.alg);
  //   let el = rhs.map_all(
  //     |o| alg.add(lhs.el.clone(), &o.el),
  //     |z| alg.add_rhs(alg.from_integer(z.clone()), &lhs.el),
  //     |l| alg.add_rhs(alg.from_integer(l), &lhs.el))
  //     .ok_or_else(|| exceptions::PyRuntimeError::new_err("failed"))?;
  //   Ok(Self::new(alg, el))
  // }
}
