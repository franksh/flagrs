#![allow(unused_imports)] // TODO: remove

use pyo3::prelude::*;

pub(crate) use rnt::*;
pub(crate) use paste::paste;

mod ugly;
mod impl_macro;
mod int_arg;
mod py_zz;

pub(crate) use int_arg::IntArg;
pub(crate) use py_zz::{PyZZ, ZZArg};
pub(crate) use ugly::RugInteger;

mod py_zn;
mod py_poly_zz;
mod py_vec_zz;
mod py_mat_zz;


/// A Python module implemented in Rust.
#[pymodule]
fn flagrs(py: Python, m: &PyModule) -> PyResult<()> {
  m.add_class::<py_zz::PyZZ>()?;

  m.add_class::<py_zn::PyZModClass>()?;
  m.add_class::<py_zn::PyZMod>()?;

  m.add_class::<py_poly_zz::PyZPolyClass>()?;
  m.add_class::<py_poly_zz::PyZPoly>()?;
  m.add_class::<py_vec_zz::PyZVecClass>()?;
  m.add_class::<py_vec_zz::PyZVec>()?;
  m.add_class::<py_mat_zz::PyZMatClass>()?;
  m.add_class::<py_mat_zz::PyZMat>()?;

  Ok(())
}
