use std::{borrow::{Borrow, Cow}, hash::{Hasher, Hash}, sync::Arc};
use pyo3::{PyErr, basic::CompareOp, exceptions::{self, PyNotImplementedError}};

use super::*;

make_py_class!(PyZMat, MatrixSpace<ZRing>, MatElem<ZZ>);

type ZMatArg<'p> = IntArg<'p,PyRef<'p,PyZMat>>;

impl PyZMat {
  // pub fn promote_zz(&self, val: impl Into<ZZ>) -> PolyElem<ZZ> {
  //   self.alg.elem(vec![val.into()])
  // }

  // fn with_arg<F,R>(&self, arg: ZMatArg, f: F) -> R
  // where F: FnOnce(&PolyElem<ZZ>) -> R {
  //   use IntArg::*;
  //   match arg {
  //     Obj(o) => f(&o.el),
  //     ZZ(o) => {
  //       let tmp = self.promote_zz(o.el.clone());
  //       f(&tmp)
  //     },
  //     Lit(l) => {
  //       let tmp = self.promote_zz(l.0);
  //       f(&tmp)
  //     },
  //   }
  // }
}

pub trait IteratorEx: Iterator {
  fn min_max(self) -> Option<(Self::Item, Self::Item)>;
}

impl<It: Iterator> IteratorEx for It
where
  It::Item: Ord + Copy,
{
  fn min_max(self) -> Option<(Self::Item, Self::Item)> {
    self.fold(None, |acc, v| match acc {
      Some((min, max)) => Some((min.min(v), max.max(v))),
      None => Some((v,v)),
    })
  }
}

#[pymethods]
impl PyZMatClass {
  #[new]
  fn create(rows: usize, cols: Option<usize>) -> Self {
    assert!(rows < 1<<30);
    Self::new(MatrixSpace::new(ZRing, rows, cols.unwrap_or(rows)))
  }

  fn __call__(&self, els: Vec<Vec<RugInteger>>) -> PyResult<PyZMat> {
    let els = unsafe { std::mem::transmute::<Vec<Vec<RugInteger>>, Vec<Vec<ZZ>>>(els) };
    match self.alg.elem_from_vecs(&els) {
      Some(el) => Ok(PyZMat::new(Arc::clone(&self.alg), el)),
      None => Err(exceptions::PyValueError::new_err("mismatched rows/columns")),
    }
  }
}


#[pymethods]
impl PyZMat {
  #[new]
  fn create() -> PyResult<Self> {
    unimplemented!()
  }

  // ObjectProtocol
  fn __str__(&self) -> String {
    self.__repr__()
  }

  fn __repr__(&self) -> String {
    "test".to_owned()
  }

  fn __bool__(&self) -> bool {
    self.alg.is_zero(&self.el)
  }

  fn __richcmp__(&self, rhs: ZMatArg, op: CompareOp) -> PyResult<bool> {
    // self.with_arg(rhs, |o| {
    //   match op {
    //     CompareOp::Eq => Ok(self.el == *o),
    //     CompareOp::Ne => Ok(self.el != *o),
    //     _ => Err(PyNotImplementedError::new_err("invalid comparison")),
    //   }
    // })
    Ok(true)
  }

  // fn __hash__(&self) -> u64 {
  //   let mut hasher = std::collections::hash_map::DefaultHasher::new();
  //   self.el.hash(&mut hasher);
  //   hasher.finish()
  // }

  // NumberProtocol
  fn __neg__(&self) -> PyResult<Self> {
    fwd_to_alg!(self neg)
  }
  fn __add__(lhs: PyRef<Self>, rhs: ZMatArg) -> PyResult<Self> {
    fwd_to_alg!(lhs rhs add add_rhs)
  }
  fn __radd__(lhs: PyRef<Self>, rhs: ZMatArg) -> PyResult<Self> {
    fwd_to_alg!(lhs rhs add_rhs add)
  }
  fn __sub__(lhs: PyRef<Self>, rhs: ZMatArg) -> PyResult<Self> {
    fwd_to_alg!(lhs rhs sub sub_rhs)
  }
  fn __rsub__(lhs: PyRef<Self>, rhs: ZMatArg) -> PyResult<Self> {
    fwd_to_alg!(lhs rhs sub_rhs sub)
  }
  // fn __mul__(lhs: PyRef<Self>, rhs: ZMatArg) -> PyResult<Self> {
  //   fwd_to_alg!(lhs rhs mul mul_rhs)
  // }
  // fn __rmul__(lhs: PyRef<Self>, rhs: ZMatArg) -> PyResult<Self> {
  //   fwd_to_alg!(lhs rhs mul_rhs mul)
  // }

  // fn __mod__(lhs: PyRef<Self>, rhs: ZMatArg) -> PyResult<Self> {
  //   fwd_to_alg!(lhs rhs rem rem_rhs)
  // }
  // fn __rmod__(lhs: PyRef<Self>, rhs: ZMatArg) -> PyResult<Self> {
  //   fwd_to_alg!(lhs rhs rem_rhs rem)
  // }
  // fn __floordiv__(lhs: PyRef<Self>, rhs: ZMatArg) -> PyResult<Self> {
  //   fwd_to_alg!(lhs rhs div div_rhs)
  // }
  // fn __rfloordiv__(lhs: PyRef<Self>, rhs: ZMatArg) -> PyResult<Self> {
  //   fwd_to_alg!(lhs rhs div_rhs div)
  // }
  // fn __divmod__(lhs: PyRef<Self>, rhs: ZMatArg) -> PyResult<(Self,Self)> {
  //   let alg = Arc::clone(&lhs.alg);
  //   let (el1,el2) = rhs.map_int(
  //     |o| alg.divrem(lhs.el.clone(), o.el.clone()),
  //     |i| alg.divrem(lhs.el.clone(), alg.from_integer(i)),
  //   ).ok_or_else(|| exceptions::PyRuntimeError::new_err("failed"))?;
  //   Ok((Self::new(Arc::clone(&lhs.alg), el1), Self::new(alg, el2)))
  // }
  // fn __rdivmod__(lhs: PyRef<Self>, rhs: ZMatArg) -> PyResult<(Self,Self)> {
  //   let alg = Arc::clone(&lhs.alg);
  //   let (el1,el2) = rhs.map_int(
  //     |o| alg.divrem_rhs(o.el.clone(), lhs.el.clone()),
  //     |i| alg.divrem_rhs(alg.from_integer(i), lhs.el.clone()),
  //   ).ok_or_else(|| exceptions::PyRuntimeError::new_err("failed"))?;
  //   Ok((Self::new(Arc::clone(&lhs.alg), el1), Self::new(alg, el2)))
  // }

  // fn __pow__(lhs: PyRef<Self>, rhs: ZZArg, modulo: Option<ZZArg>) -> PyResult<Self> {
  //   if modulo.is_some() {
  //     todo!();
  //   }
  //   lhs.alg.power(lhs.el.clone(), rhs.into())
  //           .ok_or_else(|| exceptions::PyRuntimeError::new_err("failed"))
  //           .map(|el| Self::new(Arc::clone(&lhs.alg), el))
  // }
  // __rpow__ doesn't make any sense.
}
