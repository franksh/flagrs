
use std::ops::Deref;

use pyo3::{AsPyPointer, FromPyObject, PyAny, PyErr, PyObject, PyResult, Python, ToPyObject, exceptions, ffi};
use rug::{Integer, integer::{Order, SmallInteger}};
use rnt::IntegerExt;


// pub enum IntOr<T> {
//   Small(i64),
//   Big(Integer),
//   Other(T),
// }

// impl<T> IntOr<T> {
//   pub fn try_into<U>(self) -> Option<IntOr<U>> {
//     match self {
//       IntOr::Small(v) => Some(IntOr::Small(v)),
//       IntOr::Big(v) => Some(IntOr::Big(v)),
//       IntOr::Other(_) => None,
//     }
//   }

//   pub fn int_or(self) -> Result<Integer,T> {
//     match self {
//       IntOr::Small(v) => Ok(Integer::from(v)),
//       IntOr::Big(v) => Ok(v),
//       IntOr::Other(t) => Err(t),
//     }
//   }
// }

unsafe fn inject_rug(n: &Integer) -> *mut ffi::PyObject {
  let mut signed = if n.cmp0().is_ge() { 0 } else { 1 };
  let mut buffer = n.twos_complement_bytes(true);
  ffi::_PyLong_FromByteArray(buffer.as_mut_ptr(), buffer.len(), 1, signed)
}

unsafe fn extract_rug_big(ptr: *mut ffi::PyLongObject, buffer: &mut [u8]) -> Option<Integer> {
  let res = ffi::_PyLong_AsByteArray(ptr, buffer.as_mut_ptr(), buffer.len(), 1, 1);
  if res == -1 {
    return None;
  }
  // I really hate Python's `_PyLong_AsByteArray()`. Instead of doing the sane
  // thing, i.e. give pure bits when we indicate we don't care about sign, it
  // goes out of its way to do the stupid thing. If we say we don't care about
  // sign and the number is negative, it sets an exception and refuses to give
  // us the data. If we say "OK, fine, we care about sign then," it converts
  // the number to two's complement (inverts bits and adds 1) and there's
  // nothing we can do to stop it, so then we have to convert it back.
  //
  // Any "API-legal" attempt to invert the sign beforehand might result in a
  // full copy of the number if other references are held to it.
  //
  // One hack I've seen done is to directly modify `ob_size` in the private
  // struct and then modify it back after. But that's not particularly
  // attractive.
  Some(Integer::from_digits(buffer, Order::Lsf).keep_signed_bits(8 * buffer.len() as u32))
}

unsafe fn extract_rug(num: *mut ffi::PyObject) -> Option<Integer> {
  let mut overflow = 0;
  let v = ffi::PyLong_AsLongAndOverflow(num, &mut overflow);
  if overflow == 0 {
    return Some(Integer::from(v));
  }
  let n_bits = ffi::_PyLong_NumBits(num);
  let n_bytes = if n_bits < 0 {
    return None;
  } else if n_bits == 0 {
    return Some(Integer::default());
  } else {
    (n_bits as usize + 8) / 8 // need +1 because of two's complement bullshit.
  };
  // Still need an unnecessary copy.
  if n_bytes <= 256 {
    let mut buffer = [0; 256];
    extract_rug_big(num as *mut ffi::PyLongObject, &mut buffer[..n_bytes])
  } else {
    let mut buffer = vec![0; n_bytes];
    extract_rug_big(num as *mut ffi::PyLongObject, &mut buffer)
  }
}

#[repr(transparent)]
pub struct RugInteger(pub Integer);

impl<'p> FromPyObject<'p> for RugInteger {
  fn extract(ob: &'p PyAny) -> PyResult<Self> {
    let res = unsafe {
      if ffi::PyLong_CheckExact(ob.as_ptr()) == 0 {
        let num = ffi::PyNumber_Index(ob.as_ptr());
        if num.is_null() {
          None
        } else {
          let res = extract_rug(num);
          ffi::Py_DECREF(num);
          res
        }
      } else {
        extract_rug(ob.as_ptr())
      }
    };
    res.map(RugInteger).ok_or(exceptions::PyTypeError::new_err("conversion failed"))
  }
}

impl ToPyObject for RugInteger {
  fn to_object(&self, py: Python) -> PyObject {
    unsafe {
      let ptr = inject_rug(&self.0);
      PyObject::from_owned_ptr(py, ptr)
    }
  }
}

impl pyo3::IntoPy<PyObject> for &RugInteger {
  fn into_py(self, py: Python) -> PyObject {
    (*self).to_object(py)
  }
}

impl Deref for RugInteger {
  type Target = Integer;
  fn deref(&self) -> &Self::Target {
    &self.0
  }
}



// fn extract_integer(ob: &PyAny) -> IntOr<()> {
//   unsafe {
//     let num = if ffi::PyLong_CheckExact(ob.as_ptr()) == 0 {
//       let num = ffi::PyNumber_Index(ob.as_ptr());
//       if num.is_null() {
//         return IntOr::Other(());
//       }
//       num
//     } else {
//       ob.as_ptr()
//     };
//     let mut overflow = 0;
//     let v = ffi::PyLong_AsLongAndOverflow(num, &mut overflow);
//     if overflow == 0 {
//       return IntOr::Small(v.into());
//     }
//     let n_bits = ffi::_PyLong_NumBits(num);
//     let n_bytes = if n_bits < 0 {
//       return IntOr::Other(());
//     } else if n_bits == 0 {
//       return IntOr::Big(Integer::default());
//     } else {
//       (n_bits as usize + 8) / 8
//     };
//     // Still need an unnecessary copy.
//     let res = if n_bytes <= 256 {
//       let mut buffer = [0; 256];
//       pylong_to_rug(ob, &mut buffer[..n_bytes])
//     } else {
//       let mut buffer = vec![0; n_bytes];
//       pylong_to_rug(ob, &mut buffer)
//     };
//     res.map(IntOr::Big).unwrap_or(IntOr::Other(()))
//   }
// }

// impl<'p,T: FromPyObject<'p>> FromPyObject<'p> for IntOr<T> {
//   fn extract(ob: &'p PyAny) -> PyResult<Self> {
//     if let Ok(v) = T::extract(ob) {
//       return Ok(IntOr::Other(v));
//     }
//     RugInteger::extract(ob).map(|x| IntOr::Big(x.0))
//   }
// }
