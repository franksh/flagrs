
#[macro_export]
macro_rules! make_py_class {
  ($pyname:ident, $algtype:ty, $eltype:ty) => {
    paste! {
      #[pyclass]
      pub struct [<$pyname Class>] {
        alg: Arc<$algtype>,
      }

      #[pyclass]
      pub struct [<$pyname>] {
        alg: Arc<$algtype>,
        el: $eltype,
      }

      impl [<$pyname Class>] {
        pub fn new(alg: $algtype) -> Self {
          Self { alg: Arc::new(alg) }
        }
      }

      impl [<$pyname>] {
        pub fn new(alg: Arc<$algtype>, el: $eltype) -> Self {
          Self { alg, el }
        }
      }
    }
  };
}

#[macro_export]
macro_rules! fwd_to_alg {
  ($lhs:ident $meth:ident) => { {
    let alg = Arc::clone(&$lhs.alg);
    let el = alg.$meth($lhs.el.clone())
                .ok_or_else(|| exceptions::PyRuntimeError::new_err("failed"))?;
    Ok(Self::new(alg, el))
  } };
  ($lhs:ident $rhs:ident $lhs_meth:ident $rhs_meth:ident) => { {
    let alg = Arc::clone(&$lhs.alg);
    let el = $rhs.map_int(
      |o| alg.$lhs_meth($lhs.el.clone(), &o.el),
      |i| alg.$rhs_meth(alg.from_integer(i), &$lhs.el),
    ).ok_or_else(|| exceptions::PyRuntimeError::new_err("failed"))?;
    Ok(Self::new(alg, el))
  } };
}
