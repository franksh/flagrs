
use std::cmp::Ordering;

use pyo3::exceptions::{PyIndexError, PyRuntimeError};
use pyo3::types::{PyList, PySlice};
use pyo3::{basic::CompareOp, prelude::*, types::PyBytes};
use rug::Complete;
use rug::ops::{DivRounding, RemRounding};

use rnt::{ZZ, IntegerExt};

use crate::ugly::RugInteger;

#[pyclass]
pub struct PyZZ {
  pub(crate) el: ZZ,
}

#[derive(FromPyObject)]
pub enum Either2<A,B> {
  A(A),
  B(B),
}

#[derive(FromPyObject)]
pub enum ZZArg<'p> {
  Obj(PyRef<'p,PyZZ>),
  Lit(RugInteger),
}

impl<'p> From<ZZArg<'p>> for ZZ {
  fn from(v: ZZArg<'p>) -> Self {
    match v {
      ZZArg::Obj(o) => o.el.clone(),
      ZZArg::Lit(l) => l.0,
    }
  }
}

impl<'p> AsRef<ZZ> for ZZArg<'p> {
  fn as_ref(&self) -> &ZZ {
    match self {
      ZZArg::Obj(o) => &o.el,
      ZZArg::Lit(l) => &l.0,
    }
  }
}

impl PyZZ {
  pub(crate) fn new(el: ZZ) -> Self {
    Self { el }
  }

  pub(crate) fn compare_zz(op: CompareOp, a: &ZZ, b: &ZZ) -> bool {
    match op {
      CompareOp::Gt => a > b,
      CompareOp::Ge => a >= b,
      CompareOp::Lt => a < b,
      CompareOp::Le => a <= b,
      CompareOp::Eq => a == b,
      CompareOp::Ne => a != b,
    }
  }
}

macro_rules! rug_op {
  ($lhs:ident, ! $rhs:ident / $o:ident { $($oc:tt)* } $l:ident { $($lc:tt)* }) => { {
    Ok(Self::new( match $rhs {
      ZZArg::Obj($o) => if $o.el.cmp0().is_eq() {
        return Err(PyRuntimeError::new_err("div-by-0"));
      } else { $($oc)* },
      ZZArg::Lit($l) => if $l.0.cmp0().is_eq() {
        return Err(PyRuntimeError::new_err("div-by-0"));
      } else { $($lc)* },
    } ))
  } };
  ($lhs:ident, $rhs:ident / $o:ident { $($oc:tt)* } $l:ident { $($lc:tt)* }) => { {
    Self::new( match $rhs {
      ZZArg::Obj($o) => { $($oc)* },
      ZZArg::Lit($l) => { $($lc)* },
    } )
  } };
  ($lhs:ident { $($sc:tt)* }) => { {
    Self::new( $($sc)* )
  } };
  ($lhs:ident/$cl:ident, $rhs:ident / $o:ident { $($oc:tt)* } $l:ident { $($lc:tt)* }) => { {
    let $cl = $lhs.el.clone();
    Self::new( match $rhs {
      ZZArg::Obj($o) => { $($oc)* },
      ZZArg::Lit($l) => { $($lc)* },
    } )
  } };
}

#[pymethods]
impl PyZZ {
  #[new]
  fn _new(arg: Option<ZZArg>) -> PyResult<Self> {
    Ok(match arg.unwrap_or(ZZArg::Lit(RugInteger(ZZ::default()))) {
      ZZArg::Obj(o) => Self::new(o.el.clone()),
      ZZArg::Lit(l) => Self::new(l.0),
    })
  }

  fn nbytes(&self) -> usize {
    self.el.significant_digits::<u8>()
  }

  fn nbits(&self) -> u32 {
    self.el.significant_bits()
  }
  fn bit_length(&self) -> u32 {
    self.el.significant_bits()
  }

  // ObjectProtocol
  //
  // MISSING: __format__, __getattr__, __setattr__, __delattr__

  fn __str__(&self) -> String {
    self.el.to_string()
  }

  fn __repr__(&self) -> String {
    self.el.to_string()
  }

  /// Should be compatible with CPython's hash algorithm for `int`.
  fn __hash__(&self) -> u64 {
    ZZ::from(&self.el % 0x1fffffffffffffffu64).to_u64_wrapping()
  }

  fn __bytes__(&self, py: Python) -> PyObject {
    PyBytes::new(py, self.el.twos_complement_bytes(true).as_slice()).into()
  }

  fn __richcmp__(&self, other: ZZArg, op: CompareOp) -> bool {
    let iref = match &other {
      ZZArg::Obj(o) => &o.el,
      ZZArg::Lit(l) => &l.0,
    };
    PyZZ::compare_zz(op, &self.el, iref)
  }

  fn __bool__(&self) -> bool {
    self.el.cmp0().is_ne()
  }

  // NumberProtocol
  //

  fn __index__(&self) -> &RugInteger {
    println!("transmuted");
    unsafe { std::mem::transmute(&self.el) }
  }

  fn __abs__(lhs: PyRef<PyZZ>) -> Self {
    rug_op!(lhs { lhs.el.as_abs().clone() })
  }
  fn __neg__(lhs: PyRef<PyZZ>) -> Self {
    rug_op!(lhs { lhs.el.as_neg().clone() })
  }
  fn __add__(lhs: PyRef<PyZZ>, rhs: ZZArg) -> Self {
    rug_op!(lhs, rhs / o { lhs.el.clone() + &o.el } l { &lhs.el + l.0 })
  }
  fn __radd__(lhs: PyRef<PyZZ>, rhs: ZZArg) -> Self {
    rug_op!(lhs, rhs / o { &o.el + lhs.el.clone() } l { l.0 + &lhs.el })
  }
  fn __sub__(lhs: PyRef<PyZZ>, rhs: ZZArg) -> Self {
    rug_op!(lhs, rhs / o { lhs.el.clone() - &o.el } l { &lhs.el - l.0 })
  }
  fn __rsub__(lhs: PyRef<PyZZ>, rhs: ZZArg) -> Self {
    rug_op!(lhs, rhs / o { &o.el - lhs.el.clone() } l { l.0 - &lhs.el })
  }
  fn __mul__(lhs: PyRef<PyZZ>, rhs: ZZArg) -> Self {
    rug_op!(lhs, rhs / o { lhs.el.clone() * &o.el } l { &lhs.el * l.0 })
  }
  fn __rmul__(lhs: PyRef<PyZZ>, rhs: ZZArg) -> Self {
    rug_op!(lhs, rhs / o { &o.el * lhs.el.clone() } l { l.0 * &lhs.el })
  }

  fn __floordiv__(lhs: PyRef<PyZZ>, rhs: ZZArg) -> PyResult<Self> {
    rug_op!(lhs, ! rhs / o { lhs.el.clone().div_euc(&o.el) } l { (&lhs.el).div_euc(l.0) })
  }
  fn __rfloordiv__(lhs: PyRef<PyZZ>, rhs: ZZArg) -> PyResult<Self> {
    rug_op!(lhs, ! rhs / o { o.el.clone().div_euc(&lhs.el) } l { l.0.div_euc(&lhs.el) })
  }
  fn __mod__(lhs: PyRef<PyZZ>, rhs: ZZArg) -> PyResult<Self> {
    rug_op!(lhs, ! rhs / o { lhs.el.clone().rem_euc(&o.el) } l { (&lhs.el).rem_euc(l.0) })
  }
  fn __rmod__(lhs: PyRef<PyZZ>, rhs: ZZArg) -> PyResult<Self> {
    rug_op!(lhs, ! rhs / o { o.el.clone().rem_euc(&lhs.el) } l { l.0.rem_euc(&lhs.el) })
  }

  fn __divmod__(lhs: PyRef<PyZZ>, rhs: ZZArg) -> PyResult<Self> {
    todo!()
  }
  fn __rdivmod__(lhs: PyRef<PyZZ>, rhs: ZZArg) -> PyResult<Self> {
    todo!()
  }

  fn __pow__(lhs: PyRef<PyZZ>, rhs: ZZArg, modulo: Option<ZZArg>) -> PyResult<Self> {
    todo!()
  }
  fn __rpow__(lhs: PyRef<PyZZ>, rhs: ZZArg, modulo: Option<ZZArg>) -> PyResult<Self> {
    todo!()
  }

  fn __invert__(lhs: PyRef<PyZZ>) -> Self {
    rug_op!(lhs { !lhs.el.clone() })
  }
  fn __or__(lhs: PyRef<PyZZ>, rhs: ZZArg) -> Self {
    rug_op!(lhs, rhs / o { lhs.el.clone() | &o.el } l { l.0 | &lhs.el }) // Commutative.
  }
  fn __ror__(lhs: PyRef<PyZZ>, rhs: ZZArg) -> Self {
    rug_op!(lhs, rhs / o { lhs.el.clone() | &o.el } l { l.0 | &lhs.el }) // Commutative.
  }
  fn __xor__(lhs: PyRef<PyZZ>, rhs: ZZArg) -> Self {
    rug_op!(lhs, rhs / o { lhs.el.clone() ^ &o.el } l { l.0 ^ &lhs.el }) // Commutative.
  }
  fn __rxor__(lhs: PyRef<PyZZ>, rhs: ZZArg) -> Self {
    rug_op!(lhs, rhs / o { lhs.el.clone() ^ &o.el } l { l.0 ^ &lhs.el }) // Commutative.
  }
  fn __and__(lhs: PyRef<PyZZ>, rhs: ZZArg) -> Self {
    rug_op!(lhs, rhs / o { lhs.el.clone() & &o.el } l { l.0 & &lhs.el }) // Commutative.
  }
  fn __rand__(lhs: PyRef<PyZZ>, rhs: ZZArg) -> Self {
    rug_op!(lhs, rhs / o { lhs.el.clone() & &o.el } l { l.0 & &lhs.el }) // Commutative.
  }
  fn __lshift__(lhs: PyRef<PyZZ>, rhs: ZZArg) -> PyResult<Self> {
    todo!()
  }
  fn __rlshift__(lhs: PyRef<PyZZ>, rhs: ZZArg) -> PyResult<Self> {
    todo!()
  }
  fn __rshift__(lhs: PyRef<PyZZ>, rhs: ZZArg) -> PyResult<Self> {
    todo!()
  }
  fn __rrshift__(lhs: PyRef<PyZZ>, rhs: ZZArg) -> PyResult<Self> {
    todo!()
  }

  // More misc

  /// Hamming weight.
  ///
  /// For non-negative integers, this gives the number of bits set. For negative
  /// numbers it gives the number of zeros in its two's complement form.
  fn weight(&self) -> u32 {
    if self.el.cmp0().is_ge() { self.el.count_ones().unwrap() } else { self.el.count_zeros().unwrap() }
  }

  // truncate
  // next_bit
  // square
  // double
  // kronecker
  // gcd
  // gcdext / egcd
  // root
  // sqrt
  // next_prime
  // is_prime
  // make_odd
  // small_factors

  // Sequence protocol.

  fn __len__(&self) -> PyResult<usize> {
    Ok(self.el.significant_bits() as usize)
  }

  fn __getitem__(&self, idx: Either2<&PySlice,isize>) -> PyResult<bool> {
    let bz = self.el.significant_bits() as isize;
    match idx {
      Either2::A(sl) => unimplemented!(),
      Either2::B(i) => {
        let i = if i < 0 { bz + i } else { i };
        if i < 0 || i >= bz {
          Err(PyIndexError::new_err("out of bounds"))
        } else {
          Ok(self.el.get_bit(i as u32))
        }
      }
    }
  }

  /// Returns bits (as a list of bools).
  #[args(order="\"little\"", width="1")]
  fn bits(&self, order: &str, width: usize) -> Vec<bool> {
    let u = self.el.significant_bits() as usize;
    let mut v = vec![false; u.max(width)];
    if order == "little" {
      for i in 0..u { v[i] = self.el.get_bit(i as u32); }
    } else {
      for i in 0..u { v[u.max(width)-i-1] = self.el.get_bit(i as u32); }
    }
    v
  }

  // Utility functions.

  #[pyo3(text_signature="($self, order='big', width=0)")]
  /// Gives the byte representation of an integer.
  ///
  /// Note that negative numbers are given in two's-complement format and need
  /// to store at least one extra bit which might lead to an extra byte being
  /// added.
  ///
  /// The bytes are padded with appropriately to reach the given `width`, but
  /// never shortened.
  #[args(order="\"big\"", width="0")]
  fn bytes(self_: PyRef<Self>, order: &str, width: usize) -> PyObject {
    self_.el.twos_complement_bytes_padded(order != "big", width).as_slice().into_py(self_.py())
  }

  #[pyo3(text_signature="(&self, base, order='little', min_length=1, max_length=None)")]
  /// Provides a list of the number's digits in the given base.
  ///
  /// Note that the sign is dropped so this gives the digits of the absolute
  /// value.
  ///
  /// Parameters:
  ///   base : integer
  ///
  ///     Conversion base, can be negative, but the absolute value must be
  ///     greater than 1.
  ///
  ///   order : string
  ///
  ///     Order of digits, must be "big" or "little".
  ///
  ///   min_width : non-negative integer
  ///
  ///     Minimum width of the output. The list will be padded with 0 on the
  ///     most significant side.
  ///
  ///   max_width : non-negative integer
  ///
  ///     Truncate list to this length if larger. Least significant digits will
  ///     be retained.
  #[args(order="\"little\"")]
  fn digits(&self, base: ZZArg, order: &str, min_width: Option<usize>, max_width: Option<usize>) -> PyResult<Vec<Self>> {
    let base: &ZZ = base.as_ref();
    if base.significant_bits() <= 1 {
      return Err(PyRuntimeError::new_err("invalid base"))
    }
    let mut it = self.el.digit_list(base, min_width.unwrap_or(1)..max_width.unwrap_or(usize::MAX));
    if order == "big" {
      it.reverse();
    }
    Ok(it.into_iter().map(|z| Self::new(z)).collect::<Vec<Self>>())
  }

  // Utility calculations.

  fn binomial(&self, k: u32) -> Self {
    Self::new(self.el.binomial_ref(k).complete())
  }

  #[staticmethod]
  fn lucas(n: u32) -> Self {
    Self::new(ZZ::lucas(n).into())
  }

  #[staticmethod]
  fn fibonacci(n: u32) -> Self {
    Self::new(ZZ::fibonacci(n).into())
  }

  /// Computes the factorial or multifactorial of `n`.
  ///
  /// `factorial(n,m) == prod(range(n,0,-m))`
  ///
  /// Parameters:
  ///   n : integer
  ///     starting number.
  ///   m : integer (optional, defaults to 1)
  ///     the multifactorial step-size.
  #[staticmethod]
  #[pyo3(text_signature="(n, m=1)")]
  fn factorial(n: u32, m: Option<u32>) -> Self {
    Self::new(
      if let Some(m) = m {
        ZZ::factorial_m(n, m).into()
      } else {
        ZZ::factorial(n).into()
      }
    )
  }

  /// Computes the product of all primes below or equal to `n`.
  ///
  /// Parameters:
  ///   n (int)
  #[staticmethod]
  fn primorial(n: u32) -> Self {
    Self::new(ZZ::primorial(n).into())
  }
}

/*
  def __new__(_cls, arg: Option<RsIntLike> = None) -> PyResult<Self> {
    if let Some(arg) = arg {
      let n = match arg {
        RsIntLike::Small(v) => rug::Integer::from(v),
        RsIntLike::Big(v) => v,
        RsIntLike::ZZ(v) => v.val(py).clone(),
      };
      PyZZ::create_instance(py, n)
    } else {
      PyZZ::create_instance(py, rug::Integer::default())
    }
  }


  /// Sign (-1, +1, or 0) of `self`.
  def sign(&self) -> PyResult<i32> {
    match self.val(py).cmp0() {
      Ordering::Greater => Ok(1),
      Ordering::Less => Ok(-1),
      Ordering::Equal => Ok(0),
    }
  }
  def __divmod__(lhs, rhs) -> PyResult<(Self, Self)> {
    let (q, r) = rug_op!(py, ref div_rem_euc_ref, lhs, rhs);
    Ok((PyZZ::create_instance(py, q)?, PyZZ::create_instance(py, r)?))
  }
  def __pow__(lhs, rhs, modulus) -> PyResult<Self> {
    // TODO: specialize to i_pow_u?
    let base = RsIntLike::extract(py, lhs)?.into_integer(py);
    if modulus.is_none(py) {
      let exp = u32::extract(py, rhs)?;
      return PyZZ::create_instance(py, base.pow(exp));
    }
    let modulus = RsIntLike::extract(py, modulus)?;
    let exp = RsIntLike::extract(py, rhs)?;

    let res = match (exp, modulus) {
      (RsIntLike::Small(e), RsIntLike::Small(m)) => base.pow_mod(&SmallInteger::from(e), &SmallInteger::from(m)),
      (RsIntLike::Small(e), RsIntLike::Big(m)) => base.pow_mod(&SmallInteger::from(e), &m),
      (RsIntLike::Small(e), RsIntLike::ZZ(m)) => base.pow_mod(&SmallInteger::from(e), m.val(py)),
      (RsIntLike::Big(e), RsIntLike::Small(m)) => base.pow_mod(&e, &SmallInteger::from(m)),
      (RsIntLike::Big(e), RsIntLike::Big(m)) => base.pow_mod(&e, &m),
      (RsIntLike::Big(e), RsIntLike::ZZ(m)) => base.pow_mod(&e, m.val(py)),
      (RsIntLike::ZZ(e), RsIntLike::Small(m)) => base.pow_mod(e.val(py), &SmallInteger::from(m)),
      (RsIntLike::ZZ(e), RsIntLike::Big(m)) => base.pow_mod(e.val(py), &m),
      (RsIntLike::ZZ(e), RsIntLike::ZZ(m)) => base.pow_mod(e.val(py), m.val(py)),
    };

    match res {
      Ok(val) => PyZZ::create_instance(py, val),
      Err(_) => Err(PyErr::new::<ZeroError, _>(py, "no inverse found")),
    }
  }
  def inv_mod(&self, modulus: RsIntLike) -> PyResult<Self> {
    let res = match modulus {
      RsIntLike::Small(n) => self.val(py).invert_ref(&SmallInteger::from(n)).map(Into::into),
      RsIntLike::Big(n) => self.val(py).invert_ref(&n).map(Into::into),
      RsIntLike::ZZ(n) => self.val(py).invert_ref(n.val(py)).map(Into::into),
    };
    match res {
      Some(val) => PyZZ::create_instance(py, val),
      None => Err(PyErr::new::<ZeroError, _>(py, "no inverse found")),
    }
  }

  def __invert__(&self) -> PyResult<Self> {
    PyZZ::create_instance(py, !self.val(py).clone())
  }
  def __and__(lhs, rhs) -> PyResult<Self> {
    let n = rug_op!(py, bitand, lhs, rhs);
    PyZZ::create_instance(py, n)
  }
  def __or__(lhs, rhs) -> PyResult<Self> {
    let n = rug_op!(py, bitor, lhs, rhs);
    PyZZ::create_instance(py, n)
  }
  def __xor__(lhs, rhs) -> PyResult<Self> {
    let n = rug_op!(py, bitxor, lhs, rhs);
    PyZZ::create_instance(py, n)
  }
  def __lshift__(lhs, rhs) -> PyResult<Self> {
    let b = i32::extract(py, rhs)?;
    let z = PyZZ::extract(py, lhs)?;
    PyZZ::create_instance(py, z.val(py).clone() << b)
  }
  def __rshift__(lhs, rhs) -> PyResult<Self> {
    let b = i32::extract(py, rhs)?;
    let z = PyZZ::extract(py, lhs)?;
    PyZZ::create_instance(py, z.val(py).clone() >> b)
  }

  /// Number of bits needed to represent the absolute value of `self`.
  def nbits(&self) -> PyResult<u32> {
    Ok(self.val(py).significant_bits())
  }
  /// Synonym for `self.nbits()`.
  def bit_length(&self) -> PyResult<u32> {
    self.nbits(py)
  }

  /// Number of bits set in the absolute value of `self`.
  def weight(&self) -> PyResult<u32> {
    Ok(self.val(py).as_abs().count_ones().unwrap())
  }

  /// Truncates to the specified number of bits.
  def truncate(&self, bits: u32, signed: bool = false) -> PyResult<PyZZ> {
    let z = if signed {
      self.val(py).clone().keep_signed_bits(bits)
    } else {
      self.val(py).clone().abs().keep_bits(bits)
    };
    PyZZ::create_instance(py, z)
  }

  /// Returns the next higher power of two.
  def next_bit(&self) -> PyResult<PyZZ> {
    PyZZ::create_instance(py, self.val(py).next_power_of_two_ref().into())
  }

  def __len__(&self) -> PyResult<usize> {
    Ok(self.val(py).significant_bits() as usize)
  }

  def __getitem__(&self, key: &PyObject) -> PyResult<PyObject> {
    let mut idx = i32::extract(py, key)?;
    let n = self.val(py);
    if idx < 0 {
      idx = idx - (idx % n.significant_bits() as i32)
    }
    let idx = idx as u32;
    if idx >= n.significant_bits() {
      return Err(PyErr::new::<cpython::exc::IndexError, _>(py, "not enough bits"));
    }

    let bit = n.get_bit(idx);

    Ok( bit.into_py_object(py).into_object() )
  }

  /// Returns the `d`th root of `self` as an integer and the remainder.
  ///
  /// ```
  /// >>> q,r = n.iroot(d)
  /// >>> q**d + r == n
  /// True
  /// ```
  def root(&self, n: u32) -> PyResult<(PyZZ, PyZZ)> {
    let (q, r) = self.val(py).root_rem_ref(n).into();
    Ok((PyZZ::create_instance(py, q)?, PyZZ::create_instance(py, r)?))
  }

  /// Returns `floor(sqrt(self))` and the remainder.
  def sqrt(&self) -> PyResult<(PyZZ, PyZZ)> {
    let (q, r) = self.val(py).sqrt_rem_ref().into();
    Ok((PyZZ::create_instance(py, q)?, PyZZ::create_instance(py, r)?))
  }

  def kronecker(&self, n: RsIntLike) -> PyResult<i32> {
    Ok( self.val(py).kronecker(&n.into_integer(py)) )
  }

  /// Computes the GCD of self with all the arguments.
  def gcd(&self, *args, **_kwargs) -> PyResult<PyZZ> {
    let mut g = self.val(py).clone();
    for ob in args.iter(py) {
      g = intlike_call!(py, ob, g.gcd);
    }
    PyZZ::create_instance(py, g)
  }

  def egcd(&self, other: RsIntLike) -> PyResult<(PyZZ, PyZZ, PyZZ)> {
    let (g,x,y) = match other {
      RsIntLike::Small(v) => self.val(py).gcd_cofactors_ref(&v.into()).into(),
      RsIntLike::Big(v) => self.val(py).gcd_cofactors_ref(&v).into(),
      RsIntLike::ZZ(v) => self.val(py).gcd_cofactors_ref(v.val(py)).into(),
    };
    Ok((PyZZ::create_instance(py, g)?,
        PyZZ::create_instance(py, x)?,
        PyZZ::create_instance(py, y)?))
  }

  /// Computes the LCM of self with all the arguments.
  def lcm(&self, *args, **_kwargs) -> PyResult<PyZZ> {
    let mut g = self.val(py).clone();
    for ob in args.iter(py) {
      g = intlike_call!(py, ob, g.lcm);
    }
    PyZZ::create_instance(py, g)
  }

  def next_prime(&self) -> PyResult<PyZZ> {
    PyZZ::create_instance(py, self.val(py).next_prime_ref().into())
  }

  /// Does trial division, followed by a Baille-PSW test, followed by
  /// `(reps-24)` Miller-Rabin tests.
  def is_prime(&self, reps: u32 = 25) -> PyResult<bool> {
    use rug::integer::IsPrime;
    match self.val(py).is_probably_prime(reps) {
      IsPrime::No => Ok(false),
      _ => Ok(true),
    }
  }

  /// Removes small prime factors (up to the given limit) and returns the number and a factor list.
  def small_factors(&self, upto: usize = 0x10_0000) -> PyResult<(PyZZ, Vec<(usize, u32)>)> {
    let (n, fs) = self.val(py).clone().small_factors(upto);
    Ok((PyZZ::create_instance(py, n)?, fs))
  }

  /// Removes factors of 2 and returns the odd component and the number of factors removed.
  def make_odd(&self) -> PyResult<(PyZZ, u32)> {
    let (n, e) = self.val(py).clone().make_odd();
    Ok((PyZZ::create_instance(py, n)?, e))
  }

  /// Attempts to factor `self` with Pollard's rho method.
  ///
  /// No trial division or primality testing is performed so do not use it
  /// before removing trivial factors.
  ///
  /// Performs well if the smallest factor of `self` is small compared to the
  /// others.
  ///
  /// The optional parameter specifies the maximum number of iterations and
  /// should be a power of two.
  def factor_rho(&self, upto: usize = 0x1_0000) -> PyResult<Option<PyZZ>> {
    factor_rho(self.val(py), upto).map(|x| PyZZ::create_instance(py, x)).transpose()
  }

  /// Attempts to factor `self` by Fermat's method.
  ///
  /// No trial division or primality testing is performed so do not use it
  /// before removing trivial factors.
  ///
  /// Performs well if `self` can be factored into `k*l` where `k` and `l` are
  /// roughly the same size.
  ///
  /// The optional `start` and `end` specifies the range searched:
  /// `floor(sqrt(self)) + start .. floor(sqrt(self)) + end`.
  ///
  /// Tip: if you know `n = p*q` where `s**2 * p/q ~ r` for some integer `r`,
  /// try factoring `r*n` instead.
  def factor_fermat(&self, start: i64 = 0, end: i64 = 0x1_0000) -> PyResult<Option<PyZZ>> {
    factor_fermat(self.val(py), start..end).map(|x| PyZZ::create_instance(py, x)).transpose()
  }

  // .square
  // ::random
  // ::lucas
  // ::fibonacci
  // ::factorial
  // ::binomial
  // .legendre/jacobi/kronecker
  // .is_even/.is_odd


  @property def M(&self) -> PyResult<PyAlgResZZ> {
    let m = self.val(py).clone();
    let alg = AlgRes::create(m);
    PyAlgResZZ::create_instance(py, alg)
  }
});
*/
